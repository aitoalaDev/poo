package interfaz;
import clhospital.*;
import java.util.Scanner;
/**
 * @author AndresT
 */
public class TestPaciente {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        // NUEVO PACIENTE USANDO CONSTRUCTOR CON INFO PARCIAL DEL PACIENTE
        System.out.println("Ingrese nombre del paciente: ");
        String name = sc.nextLine();
        System.out.println("Ingrese edad del paciente: ");
        int age = sc.nextInt();
        System.out.println("Ingrese genero del paciente H o M: ");
        char g = sc.next().charAt(0);
        Paciente p1 = new Paciente(name,age,g);
        // NUEVO PACIENTE USANDO CONSTRUCTOR DADO TODA LA INFO DEL PACIENTE
        System.out.print("\n------------------------\n");

        System.out.println("Ingrese nombre del paciente: ");
        String name2 = sc.next();
        System.out.println("Ingrese edad del paciente: ");
        int age2 = sc.nextInt();
        System.out.println("Ingrese genero del paciente H o M: ");
        char g2 = sc.next().charAt(0);
        System.out.println("Ingrese peso del paciente: ");
        double w = sc.nextDouble();
        System.out.println("Ingrese altura del paciente: ");
        double h = sc.nextDouble();
        Paciente p2 = new Paciente(name2,age2,g2,w,h);
        
        // METODO PARA IMPRIMIR POR PANTALLA INFORMACION DE PACIENTES
        System.out.println("------------------------");

        System.out.println(p1);
        System.out.println(p2);
        


    }

}
