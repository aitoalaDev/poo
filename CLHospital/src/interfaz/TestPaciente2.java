package interfaz;
import clhospital.*;
import java.util.Scanner;
/**
 * @author AndresT
 */
public class TestPaciente2 {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        Doctor[] doctores = new Doctor[4];
        doctores[0] = new Doctor("Jose Perez", "ginecologo", "112233");
        doctores[1] = new Doctor("Vinicio Ramos", "traumatologo", "223344");
        doctores[2] = new Doctor("Maria Rios", "cardiologo", "334455");
        System.out.println("Ingrese especialidad... mostraremos un listado de doctores asociados: ");
        String sp = sc.nextLine().toLowerCase();
        System.out.println("**LISTADO DE ESPECIALISTAS DISPONIBLES**");
        for (Doctor d : doctores) {
            if (d != null) {
                if (sp.equals(d.getSpecialty())) {
                    System.out.println(d);
                }
            }
        }
        System.out.println("\n--\nCREACION DE NUEVO PACIENTE ASIGNANDO UN DOCTOR");
        Paciente p1 = new Paciente("Andres", 20, 'H', 90, 157, doctores[2]);
        System.out.println(p1);
    }
}
