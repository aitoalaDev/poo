package clhospital;
/**
 * @author AndresT
 */
public class Paciente {
    private String name = "";
    private int age = 0;
    private String dni; 
    private char gender = 'H';
    private double weight = 0.0;
    private double height = 0.0;
    Doctor doctor;
    
    public Paciente(){
        
    }
    public Paciente(String name, int age, char g){
        this.name = name;
        this.age = age;
        this.gender = g;
        this.dni = generarDNI();
        verificarGenero(g);
        System.out.println("PACIENTE CREADO CON EXITO...");

    }
    public Paciente(String name, int age, char g, double w, double h){
        this.name = name;
        this.age = age;
        this.gender = g;
        this.weight = w;
        this.height = h;
        this.dni = generarDNI();
        verificarGenero(g);
        System.out.println("PACIENTE CREADO CON EXITO...");

    }
    public Paciente(String name, int age, char g, double w, double h, Doctor d){
        this.name = name;
        this.age = age;
        this.gender = g;
        this.weight = w;
        this.height = h;
        this.dni = generarDNI();
        verificarGenero(g);
        this.doctor = d;
        System.out.println("PACIENTE CREADO CON EXITO...");

    }
    /**
     * calcula y retorna el índice de masa corporal (IMC) del paciente. 
     * El IMC resulta de dividir la masa de una persona para su estatura 
     * elevada al cuadrado:
     * @return double
    */
    public double calcularIMC(){
        return this.weight/Math.pow(this.height,2);
    }
    
    public boolean esMayorDeEdad(){
        return this.age > 18;
    }

    private void verificarGenero(char gender){
        if (gender != 'M'){
           this.gender = 'H';
        }
    }
    @Override
    public String toString(){
        return "--Paciente--\nNombre: " +this.name+"\nEdad: "+this.age+"\nDNI: "+this.dni+"\nGenero: "+this.gender+"\nPeso: "+this.weight+"\nAltura: "+this.height+"\n\nIMC: "+calcularIMC()+"\n---\n+Info del Doctor+\n"+doctor;
    }
    private String generarDNI(){
        String ced="";
        for(int i=0; i<8; i++){
            ced += Integer.toString((int) Math.round(Math.random()));
        }
        return ced;
    }
    public void setName(String name){
        this.name = name;
    }
    public void setAge(int age){
        this.age = age;
    }
    public void setGender(char gender){
        this.gender = gender;
    }
    public void setW(double w){
        this.weight = w;
    }
    public void setH(double h){
        this.height = h;
    }
    public void setDoctor(Doctor d){
        this.doctor = d;
    }
    public Doctor getDoctor(){
        return this.doctor;
    }
}

