package clhospital;
/**
 * @author AndresT
 */
public class Doctor {
    private String name = "";
    private String specialty = "";
    private String contactNum = "";
    
    public Doctor(){
    }
    public Doctor(String n, String sp, String contact){
        this.name = n;
        this.specialty = sp;
        this.contactNum = contact;
    }
    public void setName(String name){
        this.name = name;
    }
    public void setSpecialty(String sp){
        this.specialty = sp;
    }
    public void setContactNum(String contact){
        this.contactNum = contact;
    }
    
    public String getName(){
        return this.name;
    }
    public String getSpecialty(){
        return this.specialty;
    }
    public String getContactNum(){
        return this.contactNum;
    }
    @Override
    public String toString(){
        return "++Doctor++\nNombre: " +this.name+"\nEspecialidad: "+this.specialty+"\nNumero de contacto: "+this.contactNum;
    }
}

