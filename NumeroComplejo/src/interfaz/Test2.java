package interfaz;
import java.util.Scanner;
import numerocomplejo.*;
/**
 * @author AndresT
 */
public class Test2{
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        NumeroComplejo[] arr = new NumeroComplejo[4];
        for (int i = 0; i < arr.length; i++) {
            System.out.print("\nIngrese real del numero complejo: ");
            double real = sc.nextDouble();
            System.out.print("\nIngrese imaginario del numero complejo: ");
            double imaginario = sc.nextDouble();
            arr[i] = new NumeroComplejo(real, imaginario);
        }
        int i = 1;
        for (NumeroComplejo item:arr){
            System.out.println("Numero Complejo #"+ i);
            System.out.println("(" + item.getR() + "," + item.getI() +"i)");
            System.out.println("Modulo #"+ i + ": " + item.calcularModulo());
            double r = item.productoEscalar(7).getR();
            double im = item.productoEscalar(7).getI();
            System.out.println("Prod. Escalar(Numero complejo por 7) #"+ i + ": (" + r + "," + im + "i)");
            i++;
        }
    }
}