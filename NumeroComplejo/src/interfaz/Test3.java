package interfaz;
import numerocomplejo.*;
/**
 * @author AndresT
 */
public class Test3 {
    public static void main(String[] args){
        NumeroComplejo c1 = new NumeroComplejo(2,3);
        NumeroComplejo c2 = new NumeroComplejo(2,3);
        System.out.println("C1:("+c1.getR()+","+c1.getI()+"i)");
        System.out.println("C2:("+c2.getR()+","+c2.getI()+"i)");
        NumeroComplejo s = c1.sumar(c2);
        NumeroComplejo m = c1.multiplicar(c2);
        System.out.println("Suma de c1 + c2:("+s.getR()+","+s.getI()+"i)");
        System.out.println("Multiplicacion de c1 * c2:("+m.getR()+","+m.getI()+"i)");
        System.out.println("c1 y c2 son iguales? : " + c1.sonIguales(c2));
        
    }
}

