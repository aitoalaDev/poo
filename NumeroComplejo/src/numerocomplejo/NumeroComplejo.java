package numerocomplejo;
/**
 * @author AndresT
 */
public class NumeroComplejo{

    private double real = 0;
    private double imaginario = 0;

    public NumeroComplejo(double preal, double pimaginario){
        real = preal;
        imaginario = pimaginario;
    }
    public NumeroComplejo(){
    }

    public double calcularModulo(){
        return Math.sqrt(Math.pow(real,2)+Math.pow(imaginario, 2));

    }
    public NumeroComplejo sumar(NumeroComplejo a1){
        return new NumeroComplejo(a1.real+real, a1.imaginario+imaginario);
    }
    public NumeroComplejo multiplicar(NumeroComplejo a1){
        return new NumeroComplejo((real*a1.real)-(imaginario*a1.imaginario), (real*a1.imaginario)+(imaginario*a1.real));
    }
    public boolean sonIguales(NumeroComplejo a1){
        return real==a1.real && imaginario==a1.imaginario;
    }
    /**
     * Metodo que calcula el producto escalar del numero imaginario
     * con el valr recibido como parametro.
     * @param cantidad,
     * @return un nuevo numero complejo que es el producto escalar
     */
    public NumeroComplejo productoEscalar(double cantidad){
        return new NumeroComplejo(real*cantidad, imaginario*cantidad);
    }
    public double getR(){
        return real;
    }
    public double getI(){
        return imaginario;
    }
}